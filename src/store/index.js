import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {  
    updateschedule: false,
    data_role:"",
    // url_get_API_axios:"https://onesmartaccess.ddns.net/",
    url_get_API_axios:"http://192.168.73.45:8000/",
    // url_get_API_axios:"https://onesmartaccess-laravel.herokuapp.com/",
    token_user: "",
    email_User:"",
    nickname_user:"",
    profile_user: "",
    data_Meeting:"",
    destroyer: false
  },
  mutations: {
    getupdateschedule(state, val){
      state.updateschedule = val
    },
    getRoleUser(state,val){
      state.data_role = val
    },
    getToken(state,val){
      state.token_user = val
      // console.log(val);
    },
    getEmail(state,val){
      state.email_User = val
      // console.log(val);
    },
    getNickname(state,val){
      state.nickname_user = val
    },
    getProfile(state,val){
      state.profile_user = val
    },
    getDataMeeting(state,val){
      state.data_Meeting = val
      console.log(val);
    },
    getDestroyer(state, val){
      state.destroyer = val
    }
  },
  actions: {
  },
  modules: {
  }
})
