import Vue from "vue";
import VueRouter from "vue-router";
import Navbar from "../views/Navbar";

//main
const HomePage = () => import("../views/HomePage.vue");

//component views
const CalendarMeeting = () =>
  import("@/components/CalendatMeeting/CalendarMeeting.vue");
const JoinCalendarMeeting = () =>
  import("@/components/JoinCalendarMeeting/JoinCalendarMeeting.vue");
const SchedulePerson = () =>
  import("@/components/SchedulePerson/SchedulePerson.vue");

// const Home = () => import("../views/Home.vue")

Vue.use(VueRouter);

const routes = [
  // { path: '/login',name: "login", component: login },

  {
    path: "/",
    name: "Navbar",
    component: Navbar,
    children: [
      {
        path: "/:tab",
        name: "HomePage",
        component: HomePage,
      },
      {
        path: "/CalendarMeeting",
        name: "CalendarMeeting",
        component: CalendarMeeting,
      },

      {
        path: "/SchedulePerson",
        name: "SchedulePerson",
        component: SchedulePerson,
      },
      {
        path: "/JoinCalendarMeeting",
        name: "JoinCalendarMeeting",
        component: JoinCalendarMeeting,
      },
      
    ],

    
  },
  // {
  //   path: "/",
  //   name: "CalendarMeeting",
  //   component: CalendarMeeting,
  // }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
