import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueSweetalert2 from 'vue-sweetalert2';
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import VueQrcodeReader from "vue-qrcode-reader";
import axios from 'axios'
import VueAxios from 'vue-axios'


Vue.use(VueAxios, axios)
Vue.use(VueQrcodeReader);
Vue.use(VueSweetalert2);
Vue.use(VueMaterial)
Vue.use(Antd);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')
